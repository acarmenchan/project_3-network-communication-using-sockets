import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserMapManager {

	public static Map<String, User> allKnownUserNames              = new HashMap<String, User>();
	public static Map<String, User> allCurrentlyConnectedUserNames = new HashMap<String, User>();

	public static synchronized boolean isKnownUser(String userName) {
		if (allKnownUserNames.containsKey(userName))
			return true;
		else
			return false;
	}

	public static synchronized boolean isCurrentlyConnectedUser(String userName) {
		if (allCurrentlyConnectedUserNames.containsKey(userName))
			return true;
		else
			return false;
	}

	public static synchronized User retrieveKnownUser(String userName) {
		return allKnownUserNames.get(userName);
	}

	public static synchronized User retrieveCurrentlyConnectedUser(String userName) {
		return allCurrentlyConnectedUserNames.get(userName);
	}

	public static synchronized void addKnownUser(User user) {
		allKnownUserNames.put(user.getName(), user);
	}

	public static synchronized void addCurrentlyConnectedUser(User user) {
		allCurrentlyConnectedUserNames.put(user.getName(), user);
	}

	public static synchronized ArrayList<String> getAllKnownUserNames() {

		return new ArrayList<>(allKnownUserNames.keySet());
	}

	public static synchronized ArrayList<String> getAllCurrentlyConnectedUserNames() {

		return new ArrayList<>(allCurrentlyConnectedUserNames.keySet());
	}

	public static synchronized void removeCurrentlyConnectedUser(String userName) {
		allCurrentlyConnectedUserNames.remove(userName);
	}
}
