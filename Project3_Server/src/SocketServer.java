
// By Greg Ozbirn, University of Texas at Dallas
// Adapted from example at Sun website:
// http://java.sun.com/developer/onlineTraining/Programming/BasicJava2/socket.html
// 11/07/07

import java.io.IOException;
import java.net.ServerSocket;

public class SocketServer {
	
	private ServerSocket server = null;

	public void listenSocket(int port) {
		
		try {
			this.server = new ServerSocket(port);
			System.out.println("Server running on port " + port + "," + " use ctrl-C to end");
		} catch (IOException e) {
			System.out.println("Error creating socket");
			System.exit(-1);
		}

		while (true) {
			UserConnection w;
			try {
				w = new UserConnection(this.server.accept());
				Thread t = new Thread(w);
				t.start();
			} catch (IOException e) {
				System.out.println("Accept failed");
				System.exit(-1);
			}
		}
	}

	protected void finalize() {
		try {
			this.server.close();
		} catch (IOException e) {
			System.out.println("Could not close socket");
			System.exit(-1);
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: java SocketThrdServer port");
			System.exit(1);
		}

		SocketServer server = new SocketServer();
		int port = Integer.valueOf(args[0]);
		server.listenSocket(port);
	}
}