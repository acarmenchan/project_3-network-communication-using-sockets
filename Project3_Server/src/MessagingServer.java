import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class MessagingServer {

	private String currentUserName;
	private boolean shouldQuit = false;

	public String getTimestampString() {

		Date timestamp         = new Date();
		DateFormat dateFormat  = new SimpleDateFormat("MM/dd/yy, hh:mm aaa, ");
		String timestampString = dateFormat.format(timestamp);

		return timestampString;
	}

	public boolean configureForClientName(String userName) {

		User user = null;

		if (!(UserMapManager.isKnownUser(userName))) {

			user = new User(userName);
			UserMapManager.addKnownUser(user);
			SERVER_LOG("Connection by unknown user " + user.getName());
		
		} else {

			if (UserMapManager.isCurrentlyConnectedUser(userName)) {
				
				SERVER_LOG(userName + " may not have more than one connection");
				return true;

			} else {
				user = UserMapManager.retrieveKnownUser(userName);
				SERVER_LOG("Connection by known user " + user.getName());
			}
		}

		UserMapManager.addCurrentlyConnectedUser(user);
		this.currentUserName = user.getName();
		return false;
	}

	public String getDataForUserSelection(String userRequestString) {

		String menuCommand   = parseMenuCommand(userRequestString).toLowerCase();
		String recipientName = "";
		String message       = "";

		switch (menuCommand) {

		case "a":
			return displayAllKnownUserNames();

		case "b":
			return displayAllCurrentUserNames();

		case "c":
			recipientName = parseRecipientName(userRequestString);
			message = parseMessageWithName(userRequestString);
			return sendMessageToUser(recipientName, message);

		case "d":
			message = parseMessageWithoutName(userRequestString);
			return sendMessageToAllCurrentUsers(message);

		case "e":
			message = parseMessageWithoutName(userRequestString);
			return sendMessageToAllKnownUsers(message);

		case "f":
			return displayMyMessages();

		case "g":
			return exitMenuAndQuit();

		default:
			return "Invalid selection. Try again." + "\n";
		}
	}

	private String parseMenuCommand(String userRequestString) {

		StringTokenizer tok = new StringTokenizer(userRequestString, " ");
		if (tok.hasMoreTokens()) {
			return tok.nextToken();
		}
		return "";
	}

	private String parseRecipientName(String userRequestString) {

		StringTokenizer tok = new StringTokenizer(userRequestString, " ");

		// Pop off command char
		tok.nextToken();

		// Check for user name parameter
		if (tok.hasMoreTokens()) {
			return tok.nextToken();
		}
		
		return "";
	}

	private String parseMessageWithName(String userRequestString) {

		StringTokenizer tok = new StringTokenizer(userRequestString, " ");

		// Pop off command char
		tok.nextToken();

		// Pop off user name
		tok.nextToken();

		// Return message which should be terminated by \n from client
		return tok.nextToken("\n").trim();
	}

	private String parseMessageWithoutName(String userRequestString) {

		StringTokenizer tok = new StringTokenizer(userRequestString, " ");

		// Pop off command char
		tok.nextToken();

		// Return message which should be terminated by \n from client
		return tok.nextToken("\n").trim();
	}
	
	public boolean shouldQuit() {
		return this.shouldQuit;
	}

	private void SERVER_LOG(String output) {
		System.out.println(this.getTimestampString() + output);
	}
	
	
	/*
	 * Menu Option A - Display all known user names
	 */
	public String displayAllKnownUserNames() {

		String dataString = "Known users:\n";
		int lineNumber = 1;
		for (String allKnownUsers : UserMapManager.getAllKnownUserNames()) {
			dataString += "	" + lineNumber + ". " + allKnownUsers + "\n";
			lineNumber++;
		}

		SERVER_LOG(this.currentUserName + " displays all knowns users");
		return dataString;
	}

	/*
	 * Menu Option B - Display all current user names
	 */
	public String displayAllCurrentUserNames() {

		String dataString = "Connected Users:\n";
		int lineNumber = 1;
		for (String currentUsers : UserMapManager.getAllCurrentlyConnectedUserNames()) {
			dataString += "	" + lineNumber + ". " + currentUsers + "\n";
			lineNumber++;
		}

		SERVER_LOG(this.currentUserName + " displays all connected users");
		return dataString;
	}

	/*
	 * Menu Option C - Send message to user
	 */
	public String sendMessageToUser(String recipientName, String message) {
		User recipientUser;

		if (UserMapManager.isKnownUser(recipientName)) {
			recipientUser = UserMapManager.retrieveKnownUser(recipientName);
		}

		else { 
			recipientUser = new User(recipientName);
			UserMapManager.addKnownUser(recipientUser);
		}

		recipientUser.sendMessage(message, this.currentUserName);

		SERVER_LOG(this.currentUserName + " posts a message for " + recipientUser.getName());
		return "Message posted to " + recipientUser.getName() + "\n";
	}

	/*
	 * Menu Option D - Send message to all current users
	 */
	public String sendMessageToAllCurrentUsers(String message) {

		for (String name : UserMapManager.getAllCurrentlyConnectedUserNames()) {
			
			User recipientUser = UserMapManager.retrieveCurrentlyConnectedUser(name);
			recipientUser.sendMessage(message, this.currentUserName);
		}

		SERVER_LOG(this.currentUserName + " posts a message for all currently connected users");
		return "Message posted to all currently connected users" + "\n";
	}

	/*
	 * Menu Option E - Send message to all known users
	 */
	public String sendMessageToAllKnownUsers(String message) {

		for (String name : UserMapManager.getAllKnownUserNames()) {
			
			User recipientUser = UserMapManager.retrieveKnownUser(name);
			recipientUser.sendMessage(message, this.currentUserName);
		}

		SERVER_LOG(this.currentUserName + " posts a message for all known users");
		return "Message posted to all known users" + "\n";
	}

	/*
	 * Menu Option F - Get my messages
	 */
	public String displayMyMessages() {

		User user = UserMapManager.retrieveCurrentlyConnectedUser(this.currentUserName);

		if (user.getMessages().isEmpty()) {
			return "You have no messages" + "\n";
		}

		String dataString = "Your messages:\n";

		int lineNumber = 1;
		for (Message message : user.getMessages()) {
			dataString += "	" + lineNumber + ". " + message.getFormattedMessageString() + "\n";
			lineNumber++;
		}

		// Clear all messages after retrieving them
		user.deleteAllMessages();

		SERVER_LOG(this.currentUserName + " gets messages");
		return dataString;
	}

	/*
	 * Menu Option G - Exit
	 */
	public String exitMenuAndQuit() {

		this.shouldQuit = true;
		UserMapManager.removeCurrentlyConnectedUser(this.currentUserName);

		SERVER_LOG(this.currentUserName + " exits");
		return "Exiting program...";
	}
}
