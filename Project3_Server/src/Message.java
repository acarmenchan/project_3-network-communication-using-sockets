import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {

	private Date timestamp;
	private String messageString;
	private String fromUserName;

	public Message(String messageString, String fromUserName) {
		
		this.timestamp     = new Date();
		this.messageString = messageString;
		this.fromUserName  = fromUserName;
	}

	public String getTimestampString() {
		
		DateFormat dateFormat  = new SimpleDateFormat("MM/dd/yy, hh:mm aaa");
		String timestampString = dateFormat.format(this.timestamp);
		return timestampString;
	}

	public String getMessage() {
		return this.messageString;
	}

	public String getFormattedMessageString() {
		return "From " + this.fromUserName + ", " + this.getTimestampString() + ", " + this.getMessage();
	}
}
