import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

class UserConnection implements Runnable {

	private Socket client;
	private BufferedReader in = null;
	private PrintWriter out   = null;

	public UserConnection(Socket client) {
		this.client = client;

		try {
			this.in  = new BufferedReader(new InputStreamReader(client.getInputStream()));
			this.out = new PrintWriter(client.getOutputStream(), true);
		} catch (IOException e) {
			System.out.println("in or out failed");
			System.exit(-1);
		}
	}

	public void run() {

		String userName;
		String userRequestString;
		MessagingServer messagingServer = new MessagingServer();

		try {

			// Retrieve name from client
			userName = this.in.readLine();
			boolean isUserCurrentlyConnected = messagingServer.configureForClientName(userName);

			if (isUserCurrentlyConnected) {
				this.out.println("CONNECTION_FAILED");
			} else {
				this.out.println("CONNECTION_SUCCEEDED");

				do {
					// Receive menu data from client
					userRequestString = this.in.readLine();
					String responseData = messagingServer.getDataForUserSelection(userRequestString);

					// Send back data to client
					this.out.println(responseData);
				} while (!messagingServer.shouldQuit());
			}

			// Exiting server
			this.in.close();
			this.out.close();

		} catch (IOException e) {
			System.out.println("Read failed");
			System.exit(-1);
		}

		try {
			this.client.close();
		} catch (IOException e) {
			System.out.println("Close failed");
			System.exit(-1);
		}
	}
}
