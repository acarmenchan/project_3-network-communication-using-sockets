import java.util.ArrayList;

public class User {

	private String name;
	private ArrayList<Message> messages;

	public User(String name) {
		this.name = name;
		this.messages = new ArrayList<>(100);
	}

	public String getName() {
		return this.name;
	}

	public synchronized void sendMessage(String messageString, String recipientName) {
		Message message = new Message(messageString, recipientName);
		this.messages.add(message);
	}

	public synchronized ArrayList<Message> getMessages() {
		return new ArrayList<>(this.messages);
	}
	
	public synchronized void deleteAllMessages() {
		this.messages.clear();
	}
}
