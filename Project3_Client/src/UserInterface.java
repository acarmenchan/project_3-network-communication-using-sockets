import java.util.Scanner;

public class UserInterface {

	private Scanner scanner;

	public UserInterface() {
		this.scanner = new Scanner(System.in);
	}

	public String promptForName() {
		
		System.out.print("Enter your name: ");
		String name = this.scanner.nextLine();
		return name;
	}

	public String buildServerRequestString() {

		String serverCommand = getMenuChoice();
		
		switch (serverCommand) {
		case "c":
			serverCommand += getRecipientName();
			serverCommand += getMessage();
			break;

		case "d":
		case "e":
			serverCommand += getMessage();
			break;

		default:
			break;
		}
		
		return serverCommand;
	}

	private String getRecipientName() {

		System.out.print("Enter recipient's name: ");
		return " " + this.scanner.nextLine();
	}

	private String getMessage() {

		System.out.print("Enter a message: ");
		return " " + this.scanner.nextLine();
	}

	private String getMenuChoice() {
		
		System.out.println("");
		System.out.println("========================================================");
		System.out.println("  ****    MENU    ****    MENU    ****    MENU    ****  ");
		System.out.println("========================================================");
		System.out.println("a. Display the names of all known users.");
		System.out.println("b. Display the names of all currently connected users.");
		System.out.println("c. Send a text message to a particular user.");
		System.out.println("d. Send a text message to all currently connected users.");
		System.out.println("e. Send a text message to all known users.");
		System.out.println("f. Get my messages.");
		System.out.println("g. Exit.");
		System.out.println("");
		System.out.print("Enter a menu option: ");

		return this.scanner.nextLine();
	}
}