
// By Greg Ozbirn, University of Texas at Dallas
// Adapted from example at Sun website: 
// http://java.sun.com/developer/onlineTraining/Programming/BasicJava2/socket.html
// 11/07/07
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {

	private Socket socket     = null;
	private PrintWriter out   = null;
	private BufferedReader in = null;

	public void communicate() {

		UserInterface userInterface = new UserInterface();

		// Prompt for name
		String name = userInterface.promptForName();
		// Send name to server over socket
		this.out.println(name);

		try {
			
			String connectionResponse = this.in.readLine();
			
			if (connectionResponse.equals("CONNECTION_FAILED")) {
				System.out.println(connectionResponse + ": " + name + " is already connected to the server");
				return;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		while (true) {

			// Display menu
			String serverRequestString = userInterface.buildServerRequestString();

			// Send data to server
			this.out.println(serverRequestString);
			System.out.println();

			// Receive data from server
			try {
				while (true) {
					String serverSays = this.in.readLine();
					if (serverSays == null || serverSays.isEmpty())
						break;

					System.out.println(serverSays);
				}
			} catch (IOException e) {
				System.out.println("Read failed");
				System.exit(1);
			}

			// Exit program
			if (serverRequestString.equalsIgnoreCase("g")) {
				break;
			}
			
			serverRequestString = "";
		}
	}

	public void listenSocket(String host, int port) {

		// Create socket connection
		try {
			this.socket = new Socket(host, port);
			this.out    = new PrintWriter(this.socket.getOutputStream(), true);
			this.in     = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		} catch (UnknownHostException e) {
			System.out.println("Unknown host");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("No I/O");
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage:  client_hostname port");
			System.exit(1);
		}

		String host = args[0];
		int port = Integer.valueOf(args[1]);

		SocketClient client = new SocketClient();
		client.listenSocket(host, port);
		client.communicate();
	}
}